$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/Features/createaccount.feature");
formatter.feature({
  "name": "Create Account and Buy a Product",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Check email address is valid",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is on login page",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_is_on_login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks Sign in button",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_clicks_Sign_in_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enters Email address",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.enters_Email_address()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "check email is verified successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.check_email_is_verified_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Check creating account is successfull",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is on Authentication page",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_is_on_Authentication_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user selects Title",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_selects_Title()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters First Name, Last Name, Password and Date of Birth fields",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_enters_First_Name_Last_Name_Password_and_Date_of_Birth_fields()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters Company, Address, Address (Line 2), City, State, Zip/Postal Code, Country, Additional Information, Home Phone and Mobile phone",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_enters_Company_Address_Address_Line_City_State_Zip_Postal_Code_Country_Additional_Information_Home_Phone_and_Mobile_phone(java.lang.Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks Register button",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_clicks_Register_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "check account is created successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.check_account_is_created_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Check product is added to Cart successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is on My Account page",
  "keyword": "Given "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_is_on_My_Account_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user search \"summer\" word",
  "keyword": "When "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_search_word(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user add product into Cart",
  "keyword": "And "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.user_add_product_into_Cart()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "check product is added to Cart successfully",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDefinitions.CreateAccountSteps.check_product_is_added_to_Cart_successfully()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Check user buy a product successfully",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "user is on Cart page",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "user selects an address",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "user confirm cargo and conditions",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "user selects a Payment",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "user clicks Payment button",
  "keyword": "And "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "check product is bought successfully",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
});