# TestAutomation
# Description

This project includes,
* JSON API - Integration Test of http://generator.swagger.io/api/swagger.json url
* Functional Test of HepsiBurada Dog Foods page
* Automation Testing of http://automationpractice.com/ page

# Tools & Technologies

Integration Test - Postman , Automation Testing - Selenium & Cucumber & BDD & Java & Eclipse-IDE

# Details

* Integration Test - Postman is used to send request and test script is prepared to check response code.
* Functional Test - Totally 26 test cases prepared for Hepsiburada Dog Foods page.
* Automation Testing - Test steps are created for adding a new account and searching a product.

# Comment

* Automation Test includes Unit test and report parts.
* Adding product into Cart and buying the product test steps have not been completed yet for Automation Test. 
* Bug Report part is also missing.
* Video records of executed Automation Test are added to VideosAutomationTesting folder.


